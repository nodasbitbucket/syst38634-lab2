package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {

	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
	}
	
	@Test(expected = NumberFormatException.class)
	public void testGetTotalSecondsException() {
		int totalSeconds = Time.getTotalSeconds("01:01:0a");
		assertFalse("The time provided does not match the result", totalSeconds == 3661);
	}
	
	@Test
	public void testGetTotalSecondsBoundryIn() {
		int totalSeconds = Time.getTotalSeconds("01:01:59");
		assertTrue("The time provided does not match the result", totalSeconds == 3719);
	}
	
	@Test(expected = NumberFormatException.class)
	public void testGetTotalSecondsBoundryOut() {
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		assertFalse("The time provided does not match the result", totalSeconds == 3720);
	}
}
